package com.epam.learn.tools

import android.content.Context
import android.util.Log
import android.widget.TextView
import android.widget.Toast
import java.lang.Exception

const val TAG: String = "XXX"

fun Context.toast(msg: String, duration: Int = Toast.LENGTH_SHORT) {
    Toast.makeText(this, msg, duration).show()
}

fun log(a: Any? = null, textView: TextView? = null) {
    val msg = "${Throwable().stackTrace[1]} -> ${a ?: "- null"}"
    when (a) {
        is Exception -> Log.e(TAG, msg)
        else -> Log.d(TAG, msg)
    }

    textView?.let {
        it.append(a.toString())
        it.scrollDown()
    }
}

fun Any?.logIt(textView: TextView? = null) {
    val msg = "${Throwable().stackTrace[1]} -> ${this ?: "- null"}"
    Log.d(TAG, msg)
    textView?.let {
        it.append(this.toString())
        it.scrollDown()
    }
}

fun TextView.scrollDown() {
    val scrollAmount = this.layout.getLineTop(this.lineCount) - this.height
    this.scrollTo(0, if (scrollAmount > 0) scrollAmount else 0)
}